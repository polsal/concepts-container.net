﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PK.Container
{
    class kontener : IContainer
    {
        private Dictionary<Type, komponent> implementacje;

        //konstruktor
        public kontener()
        {
            implementacje = new Dictionary<Type, komponent> { };
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var i in assembly.GetExportedTypes())
            {
                if (i.IsClass)
                {
                    Type[] interfejsy = i.GetInterfaces();

                    foreach (var j in interfejsy)
                    {
                        if (implementacje.ContainsKey(j))
                        {
                            throw new Exception("Słownik zawiera już taki interfejs");
                        }
                        else
                        {
                            implementacje[i] = (komponent)j;
                        }
                    }
                }
            }
        }

        public void Register(Type type)
        {
            if (!implementacje.ContainsKey(type))
            {
                implementacje[type] = null;
            }
            else
            {
                throw new Exception("Słownik zawiera już taki interfejs");
            }
        }

        public void Register<T>(T impl) where T : class
        {
            Type[] interfejsy = impl.GetType().GetInterfaces();

            foreach (var i in interfejsy)
            {
                if (implementacje.ContainsKey(i))
                {
                    throw new Exception("Słownik zawiera już taki interfejs");
                }
                else
                {
                    implementacje[i] = (komponent)impl;
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            Type[] interfejsy = provider.GetType().GetInterfaces();

            foreach (var i in interfejsy)
            {
                if (implementacje.ContainsKey(i))
                {
                    throw new Exception("Słownik zawiera już taki interfejs");
                }
                else
                {
                    implementacje[i] = (komponent)typeof(T);
                }
            }
        }

        public T Resolve<T>() where T : class
        {
            foreach (var i in implementacje.Keys)
            {
                if (i.IsAssignableFrom(typeof(T)))
                {
                    return (T)implementacje[i];
                }
            }
            return null;
        }

        public object Resolve(Type type)
        {
                if (implementacje.ContainsKey(type))
                {
                    return implementacje[type];  
                }
                else
                {
                    throw new UnresolvedDependenciesException("Implementacja interfejsu " + type.ToString() + " nie została znaleziona");
                }
        }

        public void rozwiazywanie_zaleznosci()
        {
            List<Type> lista_interfejsow;
            foreach (var i in implementacje.Values)
            {
                lista_interfejsow = i.required_interfaces();

                if (lista_interfejsow != null)
                {

                    foreach (Type j in lista_interfejsow)
                    {
                        if (implementacje.ContainsKey(j))
                            i.dependencies_injection(j, implementacje[j]);
                        else
                            throw new UnresolvedDependenciesException("Implementacja interfejsu " + j.ToString() + " nie została znaleziona");
                    }
                }
            }
        }    
    }
}
